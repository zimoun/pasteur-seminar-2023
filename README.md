# Toward practical transparent, verifiable and long-term reproducible research
## using Guix

# Build

```
guix time-machine -C channels.scm \
     -- shell -m manifest.scm     \
     -- rubber --pdf stournier-Guix-20231214.tex
```

+ Git: https://gitlab.com/zimoun/pasteur-seminar-2023
+ SWH: https://archive.softwareheritage.org/swh:1:dir:0f4b2e8b7472c2771bb2aaa06e9a2580f3910925
