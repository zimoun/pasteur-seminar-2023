(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit "00ff6f7c399670a76efffb91276dea2633cc130c"))
      (channel
       (name 'guix-cran)
       (url "https://github.com/guix-science/guix-cran")
       (branch "master")
       (commit "ab70c9b745a0d60a40ab1ce08024e1ebca8f61b9"))
      (channel
       (name 'custom)
       (url "https://my-forge.my-institute.xyz/my-custom-channel")
       (branch "main")
       (commit "8e61e6351510f5665d09c6debc0584b3ed218e73")))
